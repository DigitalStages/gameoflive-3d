#pragma once
#include "ofMain.h"
#include "Cell.h"
#include "ofEasyCam.h"

#define rows 64
#define columns 64
#define levels 64


class GOL{
	public:
	    GOL();
        void setup(int boardSize, int numOfCells, int _loneliness, int _overpopulate, int _reprMin, int _reprMax);
        int w, x, y, z;
        int lone, over, reMin, reMax;
        // Game of life board
        Cell board[rows][columns][levels];
        void display();
        void generate();
        void getSize();
};
