#pragma once
#include "ofMain.h"
#include "GOL.h"
#include "ofxGui.h"

class ofApp : public ofBaseApp{

    public:
        void setup();
        void update();
        void draw();
        void setupGOL();
        void keyPressed(int key);

        //General values
        int width, height, speed;
        //Game of Life values
        GOL gol;
        int loneliness, overpopulate, reprMin, reprMax;

        //GUI
        ofxPanel gui;
        ofxColorSlider oLife, nLife;
        ofxToggle fullscr;
        ofxButton snapshot, startLife;
        ofxLabel title, rules, viz;
        ofxIntSlider lone, overpop, repromn, repromx, tempo;
        bool visible, Start;
        ofTrueTypeFont font;
        int rowHeight;
        string snapSPath;

        //Camera set
        ofEasyCam cam;
        ofNode ofCamTarget;
        ofLight light;
};
