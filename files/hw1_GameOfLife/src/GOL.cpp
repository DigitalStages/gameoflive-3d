#include "GOL.h"


//-------------------------------------------------------
GOL::GOL()
{

}

void GOL::setup(int boardSize, int numOfCells, int _loneliness, int _overpopulate, int _reprMin, int _reprMax)
{
  //Set cube size in x y z
  w = boardSize / 64;

  lone = _loneliness;
  over = _overpopulate;
  reMin = _reprMin;
  reMax = _reprMax;


  for(int x = 0; x < rows; x++){
      for(int y = 0; y < columns; y++){
          for(int z = 0; z < levels; z++){
              board[x][y][z].setup(x*w, y*w, z*w, w);
          }
      }
   }
}

//--------------------------------------------------------------
void GOL::generate(){


    //save previous state
    for ( int i = 0; i < columns; i++) {
        for ( int j = 0; j < rows; j++) {
            for ( int k = 0; k < levels; k++) {
                board[i][j][k].savePrevious();
            }
        }
    }

    // Loop through every spot in our 2D array and check neighbors
    for (int x = 0; x < columns; x++) {
        for (int y = 0; y < rows; y++) {
            for (int z = 0; z < levels; z++) {

                // Add up all the states in a 3x3 surrounding grid
                int neighbors = 0;
                for (int i = -1; i <= 1; i++) {
                    for (int j = -1; j <= 1; j++) {
                        for(int k = -1; k <= 1; k++){
                            neighbors += board[(x+i+columns)%columns][(y+j+rows)%rows][(z+k+levels)%levels].previous;
                        }
                    }
                }
                // A little trick to subtract the current cell's state since
                // we added it in the above loop
                neighbors -= board[x][y][z].previous;

                // Rules of Life
                if ((board[x][y][z].state == 1) && (neighbors < lone)) board[x][y][z].newState(0);          // Loneliness
                else if ((board[x][y][z].state == 1) && (neighbors > over)) board[x][y][z].newState(0);     // Overpopulation
                else if ((board[x][y][z].state == 0) && (neighbors > reMin) && (neighbors < reMax)) board[x][y][z].newState(1);
                // else do nothing!
            }
        }
    }
}

//--------------------------------------------------------------
void GOL::display(){

    // This is the easy part, just draw the cells, fill 255 for '1', fill 0 for '0'
    for ( int i = 0; i < columns;i++) {
        for ( int j = 0; j < rows; j++) {
            for(int k = 0; k < levels; k++){
                board[i][j][k].display();
            }
        }
    }
}
