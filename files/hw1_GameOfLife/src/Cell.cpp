#include "Cell.h"

//-------------------------------------------------------
Cell::Cell()
{

}

//-------------------------------------------------------
void Cell::setup(float _x, float _y, float _z, float _w)
{
    x = _x;
    y = _y;
    z = _z;
    w = _w;
    state = int(ofRandom(2));
    previous = state;
    res = 1;
}

//--------------------------------------------------------------
void Cell::savePrevious(){
    previous = state;
}

//--------------------------------------------------------------
void Cell::newState(int s) {
    state = s;
}

//--------------------------------------------------------------
void Cell::display() {
    ofColor blue = ofColor::blue;
    ofColor green = ofColor::green;
    ofColor yellow = ofColor::yellow;

    //ofSetSphereResolution(res);
    ofSetBoxResolution(res);
    if(state == 1 & previous == 1){
        ofSetColor(255, 128, 0, 255);
        ofDrawBox(x, y, z, w, w, w);
    }
    else if(state == 1 & previous == 0){
        ofSetColor(green, 200);
        //ofSetColor(g.lerp(g, 0.1), 120);
        ofDrawBox(x, y, z, w, w, w);
    }
//    else if(state == 0 & previous == 1){
//        ofSetColor(yellow, 200);
//        //ofSetColor(yellow.lerp(yellow, 0.1), 80);
//        ofDrawBox(x, y, z, w, w, w);
//        //ofDrawSphere(x, y, z, w / 2);
//    }
//    else{
//        ofSetColor(255, 255, 255, 20);
//        ofDrawBox(x, y, z, w, w, w);
//    }
}
