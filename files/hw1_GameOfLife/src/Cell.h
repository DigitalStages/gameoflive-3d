#pragma once
#include "ofMain.h"

class Cell{

	public:
		Cell();
        void setup(float _x, float _y, float _z, float _w);

        float x, y, z, w;
        int state;
        int previous;
        int res;

        //member functions
        void savePrevious();
        void newState(int s);
        void display();
};
