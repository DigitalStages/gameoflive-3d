#include "ofApp.h"

using namespace std;

//--------------------------------------------------------------
void ofApp::setup(){


    //ofSetWindowPosition(0, 0);

    //Wait for each frame when faster than 60 fps to
    //avoid screen tearing
    ofSetVerticalSync(true);
    //ofEnableSmoothing();

    //Set background color
    ofBackground(255, 255, 255);

    width = ofGetWidth();
    height = ofGetHeight();
    //Set speed of GOL via framerate
    speed = 3;
    ofSetFrameRate(speed);

    snapSPath = "";

    loneliness = 2;
    overpopulate = 3;
    reprMin = 2;
    reprMax = 4;

    visible = true;
    Start = false;

    //This sets the camera's distance from the object
    cam.setDistance(1000);
    cam.setPosition(ofVec3f(0, 250, 3000));
    cam.removeAllInteractions();
    cam.disableMouseMiddleButton();
    cam.lookAt(ofVec3f(0));
    // Add needed mouse navigation actions in combination with shortkeys
    cam.addInteraction(ofEasyCam::TRANSFORM_TRANSLATE_XY, OF_MOUSE_BUTTON_LEFT, OF_KEY_SHIFT);
    cam.addInteraction(ofEasyCam::TRANSFORM_TRANSLATE_Z, OF_MOUSE_BUTTON_RIGHT, OF_KEY_SHIFT);
    cam.addInteraction(ofEasyCam::TRANSFORM_ROTATE, OF_MOUSE_BUTTON_LEFT);

    //Set light source
    //light.setPosition(2000, 0, 0);

    //---------------------------------------------------
    //GUI - Initialize and set gui widgets
    gui.setup();
    gui.add(title.setup("GOL 3D", "Press h to hide"));
    gui.add(rules.setup("Rules", ""));
    gui.add(lone.setup("Lonelines", loneliness, 1, 23));
    gui.add(overpop.setup("Overpopulate", overpopulate, 1, 23));
    gui.add(repromn.setup("Reproduce Min", reprMin, 1, 23));
    gui.add(repromx.setup("Reproduce Max", reprMax, 1, 23));
    gui.add(tempo.setup("Speed of GOL", speed, 1, 42));
    gui.add(oLife.setup("Life Color", ofColor(100, 100, 140), ofColor(0, 0), ofColor(255, 255)));

    gui.add(startLife.setup("Restart"));
    startLife.addListener(this, &ofApp::setupGOL);
    //Set gui font
    font.load("fonts/AvantGarde-Book.ttf", 24);
    //Set game of life
    gol.setup(width, 500, loneliness, overpopulate, reprMin, reprMax);

}

//--------------------------------------------------------------
void ofApp::update(){
    //Update GOL BOXf
    gol.generate();
}

//--------------------------------------------------------------
void ofApp::draw(){
    ofBackgroundGradient(ofColor::white, ofColor::darkCyan);
    ofEnableDepthTest();
    light.enable();
    //Start scene drawn by camera
    cam.begin();
    ofSetLineWidth(1);
    ofScale(1,-1,-1);
    ofPushMatrix();
    ofTranslate(-width / 2, -height / 2, 0);
    gol.display();
    ofPopMatrix();
    cam.end();
    //Disable
    ofDisableDepthTest();
    ofDisableLighting();
    //Draw GUI widgets
    if(visible){
        gui.draw();
    }

}

//--------------------------------------------------------------
void ofApp::setupGOL(){
    //Set speed of GOL via framerate
    ofSetFrameRate(speed);
    gol.setup(ofGetWidth(), 500, loneliness, overpopulate, reprMin, reprMax);
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    switch(key) {
        case 'N':
        case 'n':
            setupGOL();
            break;
        case ' ':
            visible = !visible;
            cout<<visible<<endl;
            break;
        case 'S':
        case 's':
            snapSPath = ofToString(loneliness) + "_" + ofToString(overpopulate) + "_" + ofToString(reprMin) + "_" + ofToString(reprMin) + ".png";
            //Save all on the screen
            ofSaveScreen(snapSPath);
            break;
        case 'F':
        case 'f':
            ofToggleFullscreen();
            width = ofGetWidth();
            height = ofGetHeight();
            break;
        case 'H':
        case 'h':
            visible = !visible;
            break;
    }
}
